# Java 4 Kids
## Kinder lernen Programmieren mit Java

Die Informatik braucht dringend Nachwuchs. Je früher Kinder die Paradigmen und
Terminologie der Programmierung kennen lernen, um so einfach haben sie es später
in Lehre und Studium.

Daher unterstützt die **Java User Group Goldstadt** (JUG PF) die regionale Jugend
durch unterschiedliche Angebote unter dem Titel *Java 4 Kids*.

Eine erste Veranstaltung soll bald stattfinden für Kinder ab 12 Jahren
mit dem Ziel, die Programmiersprache Java zu erlernen. Hierzu plant die JUG PF,
einen einfachen Flipperautomaten aus Holz zu bauen, welchem mit einem Raspberry
Pi Leben eingehaucht wird. (@gzilly)